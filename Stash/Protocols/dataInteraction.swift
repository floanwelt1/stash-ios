//
//  dataInteraction.swift
//  Stash
//
//  Created by Florian on 7/18/19.
//  Copyright © 2019 Florian Abel. All rights reserved.
//

import Foundation

protocol dataInteraction: class {
    func updateView()
}

//
//  locationInteraction.swift
//  Stash
//
//  Created by Florian on 7/18/19.
//  Copyright © 2019 Florian Abel. All rights reserved.
//

import Foundation

protocol locationInteraction: class {
    func updateLocation(currentCity: String)
}

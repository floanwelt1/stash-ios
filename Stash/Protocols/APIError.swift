//
//  APIError.swift
//  Stash
//
//  Created by Florian on 7/18/19.
//  Copyright © 2019 Florian Abel. All rights reserved.
//

import Foundation

enum APIError: Error {
    case responseProblem
    case decodingProblem
    case encodingProblem
    case otherProblem
}

//
//  FeedViewController.swift
//  Stash
//
//  Created by Florian on 7/17/19.
//  Copyright © 2019 Florian Abel. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

class DiscoverViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, MKMapViewDelegate, UIViewControllerTransitioningDelegate, dataInteraction , locationInteraction {
    // ==========================================================================================
    // Protocol stubs
    // ==========================================================================================
    //
    func updateView() {
        DispatchQueue.main.async {
            self.tableView.reloadData()
            self.refreshControl.endRefreshing()
        }
    }
    
    func updateLocation(currentCity: String) {
        locationLabel.title = currentCity
    }
    

   
    // ==========================================================================================
    // Variables and constants
    // ==========================================================================================
    //
    // Location Manager
    //let locationSingleton = LocationProvider.shared
    

    
    let transition = PopAnimator()
    
    // Table variables
    private var cellHeights: [IndexPath : CGFloat] = [:]
    private let refreshControl = UIRefreshControl()
    
    
    
    // ==========================================================================================
    // IBOutlets
    // ==========================================================================================
    //
    @IBOutlet weak var locationLabel: UINavigationItem!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var stashButton: UIButton!
    
    
    
    // ==========================================================================================
    // IBActions
    // ==========================================================================================
    //
    
    
    
    // ==========================================================================================
    // Lifecycle hooks
    // ==========================================================================================
    //
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupTableView()
        
        //setupSearchController()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setDelegates()
        self.tableView.reloadData()
    }
    
    
    
    // ==========================================================================================
    // Setup
    // ==========================================================================================
    //
    func setDelegates () {
        LocationProvider.shared.setDelegate(self)
        DataSingleton.shared.setDelegate(self)
        self.tableView.delegate = self
        self.tableView.dataSource = self
    }
    
    func setupTableView () {
        self.tableView.separatorStyle = .none
        //self.tableView.layoutMargins = UIEdgeInsets.zero
        
        if #available(iOS 10.0, *) {
            tableView.refreshControl = refreshControl
        } else {
            tableView.addSubview(refreshControl)
        }
        refreshControl.addTarget(self, action: #selector(updateData), for: .valueChanged)
        refreshControl.tintColor = UIColor(red:0.25, green:0.72, blue:0.85, alpha:1.0)
        refreshControl.attributedTitle = NSAttributedString(string: "Fresh stashs coming in...")
    }

    func setupSearchController () {
        let searchController = UISearchController(searchResultsController: nil)
        navigationItem.searchController = searchController
        //navigationItem.hidesSearchBarWhenScrolling = true
        //self.navigationController?.hidesBarsOnSwipe = true
    }
    
    
    
    
    // ==========================================================================================
    // Tableview methods
    // ==========================================================================================
    //
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return DataSingleton.shared.feedItemContainer.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item = DataSingleton.shared.feedItemContainer[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "FeedItemCell", for: indexPath) as! DiscoverFeedItemCell
        cell.selectionStyle = .none
        cell.setFeedItemCell(item: item)
        let singleTapGesture = UITapGestureRecognizer(target: self, action: #selector(singleTapped))
        singleTapGesture.numberOfTapsRequired = 1
        let doubleTapGesture = UITapGestureRecognizer(target: self, action: #selector(doubleTapped))
        doubleTapGesture.numberOfTapsRequired = 2
        cell.itemImage.addGestureRecognizer(singleTapGesture)
        cell.itemImage.addGestureRecognizer(doubleTapGesture)
        singleTapGesture.require(toFail: doubleTapGesture)
        //cell.layoutMargins = UIEdgeInsets.zero
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cellHeights[indexPath] = cell.frame.size.height
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return cellHeights[indexPath] ?? 70.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(indexPath)
        // self.performSegue(withIdentifier: "feedItemDetailSegue", sender: self)
    }
    
    @objc func updateData() {
        DataSingleton.shared.updateData()
    }
    
    @objc func singleTapped(recognizer: UITapGestureRecognizer) {
        if recognizer.state == UIGestureRecognizer.State.ended {
            let tapLocation = recognizer.location(in: self.tableView)
            if let tappedIndexPath = tableView.indexPathForRow(at: tapLocation) {
                self.performSegue(withIdentifier: "DiscoverItemDetailSegue", sender: self)
            }
        }
    }
    
    @objc func doubleTapped(recognizer: UITapGestureRecognizer) {
        // do something here
        if recognizer.state == UIGestureRecognizer.State.ended {
            let tapLocation = recognizer.location(in: self.tableView)
            if let tappedIndexPath = tableView.indexPathForRow(at: tapLocation) {
                print("doubleTap at: " + String(tappedIndexPath[1]))
//                    let starred = dataSingleton.feedItems[tappedIndexPath[1]].starred
//                    if starred == true {
//                        dataSingleton.feedItems[tappedIndexPath[1]].starred = false
//                    } else {
//                        dataSingleton.feedItems[tappedIndexPath[1]].starred = true
//                    }
//                    self.tableView.beginUpdates()
//                    self.tableView.reloadRows(at: [tappedIndexPath], with: UITableView.RowAnimation.automatic) //try other animations
//                    self.tableView.endUpdates()
            }
        }
    }
    
    func flipCard() {
        
    }
    
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
            guard
                let selectedIndexPathCell = tableView.indexPathForSelectedRow,
                let selectedCell = tableView.cellForRow(at: selectedIndexPathCell)
                    as? DiscoverFeedItemCell,
                let selectedCellSuperview = selectedCell.superview
                else {
                    return nil
            }
        
            transition.originFrame = selectedCellSuperview.convert(selectedCell.frame, to: nil)
            transition.originFrame = CGRect(
                x: transition.originFrame.origin.x + 20,
                y: transition.originFrame.origin.y + 20,
                width: transition.originFrame.size.width - 40,
                height: transition.originFrame.size.height - 40
            )
        
            transition.presenting = true
            selectedCell.frontView.isHidden = true

            return transition
    }
    
    func animationController(forDismissed dismissed: UIViewController)
        -> UIViewControllerAnimatedTransitioning? {
            transition.presenting = false
            return transition

    }


    
    // ==========================================================================================
    // Segue methods
    // ==========================================================================================
    //
    // This function is called before the segue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "DiscoverItemDetailSegue") {
            // get a reference to the second view controller
            let discoverItemDetailViewController = segue.destination as! DiscoverItemDetailViewController
            
            // get reference to selectedCell
            guard let indexPath = tableView.indexPathForSelectedRow else { return }
            
            // set a variable in the second view controller with the data to pass
            discoverItemDetailViewController.transitioningDelegate = self
            discoverItemDetailViewController.feedItem = DataSingleton.shared.feedItemContainer[indexPath.row]
            
        }
    }
   
}

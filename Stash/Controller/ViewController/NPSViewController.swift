//
//  NPSViewController.swift
//  Stash
//
//  Created by Florian on 8/2/19.
//  Copyright © 2019 Florian Abel. All rights reserved.
//

import UIKit

class NPSViewController: UIViewController {

    @IBOutlet weak var outlineView: UIView!
    @IBOutlet weak var ratingLabel: UILabel!
    @IBOutlet weak var ratingSlider: UISlider!
    @IBOutlet weak var submitButton: UIButton!
    
    @IBAction func sliderAction(_ sender: Any) {
        if !sliderMoved {
            sliderMoved = true
            submitButton.isEnabled = true
        }
        sliderValue = Int(self.ratingSlider.value)
        ratingSlider.setValue(Float(sliderValue), animated: true)
        self.ratingLabel.text = String(sliderValue)
    }
    
    @IBAction func cancelAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func submitAction(_ sender: Any) {
        AuthenticationProvider.shared.sendNPSRating(npsRating: sliderValue)
        self.dismiss(animated: true, completion: nil)
    }
    
    var sliderValue = 0
    var sliderMoved = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        AuthenticationProvider.shared.askedForNps()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        styleNPSAlert()
        
    }
    
    func styleNPSAlert () {
        outlineView.layer.cornerRadius = 20
        ratingSlider.minimumValue = 0
        ratingSlider.maximumValue = 10
        ratingSlider.isContinuous = true
        blurBackground()
    }
    
    func blurBackground() {
        if !UIAccessibility.isReduceTransparencyEnabled {
            view.backgroundColor = .clear
            
            let blurEffect = UIBlurEffect(style: .regular)
            let blurEffectView = UIVisualEffectView(effect: blurEffect)
            //always fill the view
            blurEffectView.frame = self.view.bounds
            blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            
            view.insertSubview(blurEffectView, at: 0)
            //addSubview(blurEffectView) //if you have more UIViews, use an insertSubview API to place it where needed
        } else {
            view.backgroundColor = .clear
        }
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

//
//  PostStashObjectViewController.swift
//  Stash
//
//  Created by Florian on 8/12/19.
//  Copyright © 2019 Florian Abel. All rights reserved.
//

import UIKit

class PostStashObjectViewController: UIViewController {

    @IBAction func shareStashObject(_ sender: UIView) {
        guard let SOId = SOId else { return }
        print("1")
        guard let SOImage = SOImage else { return }
        print("2")
        guard let SOUrl = URL(string: "https://www.stash.love/" + SOId) else { return}
        print("3")
        guard let stashObjectTitle = SOTitle else { return }
        print("4")
        let objectsToShare = [SOImage]
        let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
        
        //Excluded Activities
        activityVC.excludedActivityTypes = [UIActivity.ActivityType.airDrop, UIActivity.ActivityType.addToReadingList]
        //
        
        activityVC.popoverPresentationController?.sourceView = sender
        self.present(activityVC, animated: true, completion: nil)
        //self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func dismissView(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBOutlet weak var shareButton: UIButton!
    
    @IBOutlet weak var dismissButton: UIButton!
    
    var SOId: String?
    var SOType: String?
    var SOTitle: String?
    var SOSubtitle: String?
    var SODescription: String?
    var SOImage: UIImage?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        styleButtons()
        // Do any additional setup after loading the view.
    }
    
    func styleButtons() {
        shareButton.layer.borderWidth = 1.0
        shareButton.layer.cornerRadius = 5
        shareButton.layer.borderColor = UIColor.blue.cgColor
        dismissButton.layer.borderWidth = 1.0
        dismissButton.layer.cornerRadius = 5
        
        dismissButton.layer.borderColor = UIColor.blue.cgColor
    }
    
    func shareStashObjectExternally (sender: UIView) {
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

//
//  CreateStashObjectViewController.swift
//  Stash
//
//  Created by Florian on 8/5/19.
//  Copyright © 2019 Florian Abel. All rights reserved.
//

import UIKit

class CreateStashObjectViewController: UIViewController {

    @IBAction func cancelCreateStashObject(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBOutlet weak var navigationBar: UINavigationItem!
    @IBOutlet weak var sightsButton: UIButton!
    @IBOutlet weak var diningButton: UIButton!
    @IBOutlet weak var eventsButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationBar.title = "Add Place"
        styleButtons()
        // Do any additional setup after loading the view.
    }
    
    func styleButtons() {
        sightsButton.layer.borderWidth = 1.0
        sightsButton.layer.cornerRadius = 5
        sightsButton.layer.borderColor = UIColor.blue.cgColor
        diningButton.layer.borderWidth = 1.0
        diningButton.layer.cornerRadius = 5
        diningButton.layer.borderColor = UIColor.blue.cgColor
        eventsButton.layer.borderWidth = 1.0
        eventsButton.layer.cornerRadius = 5
        eventsButton.layer.borderColor = UIColor.blue.cgColor
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "SightsAndLandmarksSegue" {
            if let fillInfoVC: FillInfoOfStashObjectViewController = segue.destination as? FillInfoOfStashObjectViewController{
                fillInfoVC.SOType = "SightsAndLandmarks"
            }
        } else if segue.identifier == "DiningAndEntertainmentSegue" {
            if let fillInfoVC: FillInfoOfStashObjectViewController = segue.destination as? FillInfoOfStashObjectViewController{
                fillInfoVC.SOType = "DiningAndEntertainment"
            }
        } else if segue.identifier == "EventsAndNightlifeSegue" {
            if let fillInfoVC: FillInfoOfStashObjectViewController = segue.destination as? FillInfoOfStashObjectViewController{
                fillInfoVC.SOType = "EventsAndNightLife"
            }
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

//
//  StashViewController.swift
//  Stash
//
//  Created by Florian on 7/29/19.
//  Copyright © 2019 Florian Abel. All rights reserved.
//

import Foundation
import UIKit

class SetStashViewController: UIViewController, UITextViewDelegate, stashPictureInteraction{
    func setStashPicture(image: UIImage) {
        imageView.image = image
    }
    
    
    @IBAction func cancelStash(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func stashAction(_ sender: Any) {
        setStash(alert: UIAlertAction())
    }
    @IBOutlet weak var navigationBar: UINavigationItem!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var stashButton: UIButton!
    @IBOutlet weak var keyboardView: UIView!
    @IBOutlet weak var keyboardViewHeight: NSLayoutConstraint!
    
    // Variables
    var currentStashObjectPreview: StashObjectPreview?
    var activeTextView: UITextView?
    var keyboardHeight: CGFloat?
    
    //TapGestureRecognizer
    var singleTapGestureRecognizer: UITapGestureRecognizer?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let currentStashObjectPreview = currentStashObjectPreview {
            navigationBar.title = currentStashObjectPreview.title
        }
        styleView()
        setupKeyboard()
        setupSingleTapRecognizer()
    // Do any additional setup after loading the view.
    }
    
    func styleView() {
        imageView.layer.cornerRadius = 5
        textView.layer.cornerRadius = 5
        textView.layer.borderColor = UIColor.gray.withAlphaComponent(0.5).cgColor
        textView.layer.borderWidth = 0.5
        textView.clipsToBounds = true
        textView.text = "What makes this place special to you?"
        textView.textColor = UIColor.lightGray
        
        stashButton.layer.borderWidth = 1.0
        stashButton.layer.cornerRadius = 5
        stashButton.layer.borderColor = UIColor.blue.cgColor
    }
    
    func setupKeyboard() {
        // listen to keyboard show event
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        // listen to keyboard hide event
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    func setupSingleTapRecognizer () {
        let singleTapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(singleTap))
        singleTapGestureRecognizer.numberOfTapsRequired = 1
        singleTapGestureRecognizer.isEnabled = true
        singleTapGestureRecognizer.cancelsTouchesInView = false
        self.singleTapGestureRecognizer = singleTapGestureRecognizer
    }
    
    @objc func singleTap(sender: UITapGestureRecognizer) {
        self.view.endEditing(true)
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    
    // keyboard shown
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardFrame: NSValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
            let keyboardRectangle = keyboardFrame.cgRectValue
            keyboardViewHeight.constant = keyboardRectangle.height
        } else {
            keyboardViewHeight.constant = 260.0
        }
        if let singleTapGestureRecognizer = singleTapGestureRecognizer {
            scrollView.addGestureRecognizer(singleTapGestureRecognizer)
        }
        keyboardViewHeight.constant = 260.0
        self.view.layoutIfNeeded()
        scrollView.scrollRectToVisible(keyboardView.frame, animated: true)
        if activeTextView != nil {
            scrollView.scrollRectToVisible(keyboardView.frame, animated: true)
        }
    }
    
    // keyboard hidden
    @objc func keyboardWillHide(notification: NSNotification) {
        if let singleTapGestureRecognizer = singleTapGestureRecognizer {
            scrollView.removeGestureRecognizer(singleTapGestureRecognizer)
        }
        keyboardViewHeight.constant = 0.0
        self.view.layoutIfNeeded()
    }
    
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        self.activeTextView = textView
        return true
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        self.activeTextView = nil
    }
    
//    func moveElementAboveKeyboard (element: CGRect) {
//        print(element)
//
//        if let keyboardHeight = keyboardViewHeight?.constant {
//            print(keyboardHeight)
//            //let oldBottomConstraint = bottomConstraint.constant
//            //bottomConstraint.constant = keyboardHeight + oldBottomConstraint
//            let scrollRect = CGRect(x: element.origin.x, y: element.origin.y + keyboardHeight, width: element.width, height: keyboardHeight * 2)
//            self.scrollView.scrollRectToVisible(scrollRect, animated: true)
//            self.scrollView.setContentOffset(element.origin, animated: true)
//            print("scrolled")
//        }
//    }
    
    func setStash(alert: UIAlertAction!) {
        if let stashObjectId = currentStashObjectPreview?.stashObjectId, let currentLocation = LocationProvider.shared.currentLocation {
            stashButton.isEnabled = false
            //let newStash = StashToJSON(stashObjectId: stashObjectId, message: textView.text, location: currentLocation, image: imageView.image)
            let newStash = Stash(stashObjectId: stashObjectId, location: currentLocation)
            DataSingleton.shared.addNew(stash: newStash, completion: ({ result in
                switch result {
                case .success(_):
                    self.stashButton.isEnabled = true
                    self.dismiss(animated: true, completion: nil)
                    //self.stashSetAlert(true);
                case .failure(let error):
                    print("Could not post stash Object")
                    print(error)
                    self.stashSetAlert(false);
                    self.stashButton.isEnabled = true
                }
            }))
        }
        
        self.dismiss(animated: true, completion: nil)
    }
    
    func stashSetAlert(_ success: Bool) {
        if success {
            let alert = UIAlertController(title: "Stash has been set", message: "Thank you for being part of the community :-)", preferredStyle: .alert)
            //alert.addAction(UIAlertAction(title: "Share", style: .default, handler: self.setStash))
            alert.addAction(UIAlertAction(title: "Dismiss", style: .default, handler: nil))
            DispatchQueue.main.async {
                self.present(alert, animated: true)
            }
        } else {
            let alert = UIAlertController(title: "Stash could not be set", message: "We apologize, but something went wrong :-( Please try again", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
            alert.addAction(UIAlertAction(title: "Try again", style: .default, handler: self.setStash))
            DispatchQueue.main.async {
                self.present(alert, animated: true)
            }
        }
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "TakePictureForStashSegue" {
            if let takePictureVC: TakePictureViewController = segue.destination as? TakePictureViewController{
                takePictureVC.delegate = self
            }
        }
    }

}

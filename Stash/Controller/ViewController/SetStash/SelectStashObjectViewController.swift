//
//  SelectStashObjectViewController.swift
//  Stash
//
//  Created by Florian on 8/14/19.
//  Copyright © 2019 Florian Abel. All rights reserved.
//

import Foundation
import UIKit

class SelectStashObjectViewController: UIViewController, UITableViewDelegate, UITableViewDataSource{
    
    @IBOutlet weak var navigationBar: UINavigationItem!
    @IBOutlet weak var tableView: UITableView!
    
    
    // Variables
    var stashObjectsAround: [StashObject]?
    // Table variables
    private var cellHeights: [IndexPath : CGFloat] = [:]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationBar.title = "Select stash"
        setupTableView()
        fetchObjectsAround()
        // Do any additional setup after loading the view.
    }
    
    
    // ==========================================================================================
    // Setup
    // ==========================================================================================
    //
    
    func setupTableView () {
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.separatorStyle = .none
        self.tableView.reloadData()
    }
    
    func fetchObjectsAround() {
        let stashObjectsAround = DataSingleton.shared.stashObjectContainer.filter({return $0.distance <= 250.0})
        self.stashObjectsAround = stashObjectsAround
    }
    
    
    // ==========================================================================================
    // Tableview methods
    // ==========================================================================================
    //
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let stashObjectsAround = stashObjectsAround else { return 0 }
        print(stashObjectsAround.count)
        return stashObjectsAround.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let stashObject: StashObject = stashObjectsAround![indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "StashObjectSelectCell", for: indexPath) as! StashObjectSelectCell
        cell.selectionStyle = .none
        cell.setStashObjectSelectCell(stashObject: stashObject)
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cellHeights[indexPath] = cell.frame.size.height
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return cellHeights[indexPath] ?? 70.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.performSegue(withIdentifier: "SetStashAtObjectSegue", sender: self)
    }
    
    @objc func updateFeed() {
        //DataSingleton.shared.updateFeedItems()
    }
    
    @objc func doubleTapped(recognizer: UITapGestureRecognizer) {
        // do something here
        if recognizer.state == UIGestureRecognizer.State.ended {
            let tapLocation = recognizer.location(in: self.tableView)
            if let tappedIndexPath = tableView.indexPathForRow(at: tapLocation) {
                
                
                self.tableView.beginUpdates()
                self.tableView.reloadRows(at: [tappedIndexPath], with: UITableView.RowAnimation.automatic) //try other animations
                self.tableView.endUpdates()
            }
        }
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}

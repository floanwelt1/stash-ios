//
//  StashObjectSelectTableViewCell.swift
//  Stash
//
//  Created by Florian on 8/5/19.
//  Copyright © 2019 Florian Abel. All rights reserved.
//

import UIKit

class StashObjectSelectCell: UITableViewCell {

    var stashObject: StashObject?

    @IBOutlet weak var stashObjectNameLabel: UILabel!
    
    func setStashObjectSelectCell(stashObject: StashObject) {
        //self.stashObject = stashObject
        stashObjectNameLabel.text = stashObject.title
    }

}

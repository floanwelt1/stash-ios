//
//  FeedItemCell.swift
//  Stash
//
//  Created by Florian on 7/17/19.
//  Copyright © 2019 Florian Abel. All rights reserved.
//

import Foundation
import UIKit
import MapKit
import CoreLocation

class DiscoverFeedItemCell: UITableViewCell, MKMapViewDelegate {

    
    @IBAction func flipToFrontAction(_ sender: Any) {
        UIView.transition(from: backView, to: frontView,
                          duration: 0.8, options: [.transitionFlipFromRight,
                                                   .showHideTransitionViews]) { _ in
                                                    self.backView.isUserInteractionEnabled = false
                                                    self.frontView.isUserInteractionEnabled = true
        }
        self.item?.isFrontView = true
    }
    
    @IBAction func flipToBackAction(_ sender: Any) {
        
        UIView.transition(from: frontView, to: backView,
                          duration: 0.8, options: [.transitionFlipFromLeft,
                                                   .showHideTransitionViews]) { _ in
                                                    self.frontView.isUserInteractionEnabled = false
                                                    self.backView.isUserInteractionEnabled = true
        }
        self.item?.isFrontView = false
    }
    
    @IBOutlet weak var frontView: UIView!
    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var mapViewContainer: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var titleLabelBack: UILabel!
    
    @IBOutlet weak var subtitleLabel: UILabel!
    @IBOutlet weak var subtitleLabelBack: UILabel!
    @IBOutlet weak var categoryLabel: UILabel!
    @IBOutlet weak var categoryLabelBack: UILabel!
    @IBOutlet weak var distanceLabel: UILabel!
    @IBOutlet weak var distanceLabelBack: UILabel!
    @IBOutlet weak var stashScoreLabel: UILabel!
    @IBOutlet weak var stashScoreLabelBack: UILabel!
    @IBOutlet weak var itemImage: UIImageView!
    @IBOutlet weak var favoriteImage: UIImageView!
    @IBOutlet weak var mapView: MKMapView!
    
    
    var item: FeedItem?
    let regionRadius: CLLocationDistance = 500
    
    func setFeedItemCell(item: FeedItem) {
        self.item = item
        if item.isFrontView {
            self.backView.isHidden = true
            self.backView.isUserInteractionEnabled = false
            self.frontView.isHidden = false
            self.frontView.isUserInteractionEnabled = true
        } else if !item.isFrontView {
            self.backView.isHidden = false
            self.backView.isUserInteractionEnabled = true
            self.frontView.isHidden = true
            self.frontView.isUserInteractionEnabled = false
        }
      
        
        frontView.layer.cornerRadius = 10
        frontView.layer.masksToBounds = false
        frontView.layer.shadowColor = UIColor.black.cgColor
        frontView.layer.shadowOffset = CGSize(width: 0.0, height: 3.0)
        frontView.layer.shadowRadius = 10
        frontView.layer.shadowOpacity = Float(0.3)
        itemImage.layer.cornerRadius = 10
        itemImage.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner]
        titleLabel.text = item.title
        subtitleLabel.text = item.subtitle
        categoryLabel.text = item.category
        stashScoreLabel.text = String(item.stashScore)
        if item.distance > 999 {
            let distance = (round((item.distance/1000) * 10) / 10)
            distanceLabel.text = String(distance) + " km"
        } else {
            distanceLabel.text = String(Int(item.distance)) + " m"
        }
        if item.starred {
            favoriteImage.isHidden = false
        } else if !item.starred {
            favoriteImage.isHidden = true
        }
        if let image = item.titleImage?.image {
            itemImage.image = image
        }
        
        backView.layer.cornerRadius = 10
        backView.layer.masksToBounds = false
        backView.layer.shadowColor = UIColor.black.cgColor
        backView.layer.shadowOffset = CGSize(width: 0.0, height: 3.0)
        backView.layer.shadowRadius = 10
        backView.layer.shadowOpacity = Float(0.3)
        mapViewContainer.layer.cornerRadius = 10
        mapViewContainer.layer.masksToBounds = true
        mapViewContainer.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner]
        titleLabelBack.text = item.title
        subtitleLabelBack.text = item.subtitle
        categoryLabelBack.text = item.category
        stashScoreLabelBack.text = String(item.stashScore)
        if item.distance > 999 {
            let distance = (round((item.distance/1000) * 10) / 10)
            distanceLabelBack.text = String(distance) + " km"
        } else {
            distanceLabelBack.text = String(Int(item.distance)) + " m"
        }
        let coordinateRegion = MKCoordinateRegion(center: item.location.coordinate, latitudinalMeters: item.distance * 3, longitudinalMeters: item.distance * 3)
        self.mapView.delegate = self
        self.mapView.showsUserLocation = true
        self.mapView.removeAnnotations(self.mapView.annotations)
        let mapPoint = MapPoint(title: item.title, subtitle: item.subtitle, type: item.category, coordinate: item.location.coordinate)
        self.mapView.addAnnotation(mapPoint)
        self.mapView.setRegion(coordinateRegion, animated: true)
    }
}

//
//  TableViewCell.swift
//  Stash
//
//  Created by Florian on 8/20/19.
//  Copyright © 2019 Florian Abel. All rights reserved.
//
import Foundation
import UIKit

class SavedStashTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var stashImage: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func setSavedStashTableCell(stash: Stash) {
        guard let stashObject = DataSingleton.shared.stashObjectContainer.first(where: {$0.stashObjectId == stash.stashObjectId}) else { return }
        if (stash.images.count > 0), let image = stash.images[0].image {
            stashImage.image = image
        } else if (stashObject.images.count > 0), let image = stashObject.images[0].image {
            stashImage.image = image
        }
        titleLabel.text = stashObject.title
    }
}

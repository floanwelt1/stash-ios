//
//  ImageRequest.swift
//  Stash
//
//  Created by Florian on 8/13/19.
//  Copyright © 2019 Florian Abel. All rights reserved.
//

import Foundation

class ImageRequest: Encodable{
    var imageIds: [String]?
    
    init(imageIds: [String]){
        self.imageIds = imageIds
    }
    
}

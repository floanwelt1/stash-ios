//
//  NPSRating.swift
//  Stash
//
//  Created by Florian on 8/3/19.
//  Copyright © 2019 Florian Abel. All rights reserved.
//

import Foundation
class NPSRating: Codable{
    private var npsRating: Int
    private var npsComment: String?
    private var ratingTimestamp: Int?
    
    init(npsRating: Int) {
        self.npsRating = npsRating
    }
    
    init(npsRating: Int, npsComment: String) {
        self.npsRating = npsRating
        self.npsComment = npsComment
    }
    
    func setRatingTimestamp(timestamp: Int) {
        self.ratingTimestamp = timestamp
    }
}

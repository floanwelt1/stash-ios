//
//  UserToJSON.swift
//  Stash
//
//  Created by Florian on 8/7/19.
//  Copyright © 2019 Florian Abel. All rights reserved.
//

import Foundation
import UIKit

class UserToJSON: Encodable {
    var userId: String
    var email: String
    var token: String
    var username: String?
    var profileImage: String?
    
    
    init(user: User) {
        self.userId = user.userId!
        self.email = user.email!
        self.token = user.token!
        if let username = user.username {
            self.username = username
        }
        if let profileImage = user.profileImage {
            let imageData = profileImage.jpegData(compressionQuality: 1)
            self.profileImage = (imageData?.base64EncodedString(options: .lineLength64Characters))!
        }
    }
    
    func setUserName(username: String) {
        self.username = username
    }
    
    func setToken(token: String) {
        self.token = token
    }
}

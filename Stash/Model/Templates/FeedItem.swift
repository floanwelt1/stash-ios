//
//  File.swift
//  Stash
//
//  Created by Florian on 7/17/19.
//  Copyright © 2019 Florian Abel. All rights reserved.
//

import Foundation
import UIKit
import CoreLocation

class FeedItem {
    
    var feedItemId: UUID
    var stashObject: StashObject
    var stash: Stash?
    var title: String
    var subtitle: String
    var description: String
    var category: String
    var location: CLLocation
    var distance: CLLocationDistance
    var stashScore: Int
    var titleImage: Image?
    var stashs: [Stash] = []
    var images: [Image] = []
    var isFrontView: Bool = true
    var starred: Bool
    
    init(stashObject: StashObject) {
        self.stashObject = stashObject
        self.feedItemId = UUID()
        self.title = stashObject.title
        self.subtitle = stashObject.subtitle
        self.description = stashObject.description
        self.category = stashObject.type
        self.location = stashObject.location
        self.distance = 0
        if let stashScore = stashObject.stashScore {
            self.stashScore = Int(stashScore / 100)
        } else {
            self.stashScore = 0
        }
        self.starred = false
    }
    
    func calculateDistance(from location: CLLocation) {
        self.distance = self.location.distance(from: location)
    }
    
    func addImage(image: Image) {
        self.images.append(image)
    }
    
    func setMainImage(image: Image) {
        self.titleImage = image
    }
    
    func setStash(stash: Stash) {
        self.stashs.append(stash)
    }
    
    func update(stashObject: StashObject) {
        self.stashObject = stashObject
        self.title = stashObject.title
        self.subtitle = stashObject.subtitle
        self.description = stashObject.description
        self.category = stashObject.type
        self.location = stashObject.location
        if let stashScore = stashObject.stashScore {
            self.stashScore = Int(stashScore / 100)
        } else {
            self.stashScore = 0
        }
    }
    
    func starItem() {
        if starred {
            starred = false
        } else if !starred {
            starred = true
        }
    }
}

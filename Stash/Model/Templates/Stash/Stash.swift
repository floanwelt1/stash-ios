//
//  Stash.swift
//  Stash
//
//  Created by Florian on 7/30/19.
//  Copyright © 2019 Florian Abel. All rights reserved.
//

import Foundation
import UIKit
import CoreLocation

class Stash {
    var stashId: String?
    var stashObjectId: String?
    var userId: String?
    var message: String?
    var latitude: Double
    var longitude: Double
    var creationTimestamp: Int?
    var lastUpdatedTimestamp: Int
    var expirationTimestamp: Int?
    var imageIds: [String] = []
    var images: [Image] = []
    
    init(stashFromJSON: StashFromJSON) {
        self.stashId = stashFromJSON.stashId
        self.stashObjectId = stashFromJSON.stashObjectId
        self.userId = stashFromJSON.userId
        self.message = stashFromJSON.message
        self.latitude = stashFromJSON.latitude
        self.longitude = stashFromJSON.longitude
        self.creationTimestamp = stashFromJSON.creationTimestamp
        self.lastUpdatedTimestamp = stashFromJSON.lastUpdatedTimestamp
        self.expirationTimestamp = stashFromJSON.expirationTimestamp
        if let imageIds = stashFromJSON.imageIds {
            self.imageIds = stashFromJSON.imageIds!
        }
    }
    
    init(stashObjectId: String, location: CLLocation) {
        self.stashObjectId = stashObjectId
        self.latitude = location.coordinate.latitude
        self.longitude = location.coordinate.longitude
        self.lastUpdatedTimestamp = 0
    }
    
    func update(stash: Stash) {
        if self.stashId == stash.stashId {
            self.stashObjectId = stash.stashObjectId
            self.userId = stash.userId
            self.message = stash.message
            self.latitude = stash.latitude
            self.longitude = stash.longitude
            self.creationTimestamp = stash.creationTimestamp
            self.lastUpdatedTimestamp = stash.lastUpdatedTimestamp
            self.expirationTimestamp = stash.expirationTimestamp
            self.imageIds = stash.imageIds
        }
    }
    
    func update(stashResponse: BackendWriteResponse) {
        
    }
    
    
//    init(stashId: String, stashObjectId: String, userId: String, message: String?, latitude: Double, longitude: Double, creationTimestamp: Int, expirationTimestamp: Int, imageIds: [String]?) {
//        self.stashId = stashId
//        self.stashObjectId = stashObjectId
//        self.userId = userId
//        self.message = message
//        self.latitude = latitude
//        self.longitude = longitude
//        self.creationTimestamp = creationTimestamp
//        self.expirationTimestamp = expirationTimestamp
//        self.imageIds = imageIds!
//    }
//
//    init(stashId: String, userId: String, message: String?, latitude: Double, longitude: Double, creationTimestamp: Int, expirationTimestamp: Int, imageIds: [String]?) {
//        self.stashId = stashId
//        self.userId = userId
//        self.message = message
//        self.latitude = latitude
//        self.longitude = longitude
//        self.creationTimestamp = creationTimestamp
//        self.expirationTimestamp = expirationTimestamp
//        self.imageIds = imageIds!
//    }
    
    func addImage(image: Image) {
        self.images.append(image)
    }
    
    
}

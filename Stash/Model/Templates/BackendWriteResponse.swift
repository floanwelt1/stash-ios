//
//  WriteResponse.swift
//  Stash
//
//  Created by Florian on 8/23/19.
//  Copyright © 2019 Florian Abel. All rights reserved.
//

import Foundation

struct BackendWriteResponse: Decodable{
    var stashObjectId: String
    var stashId: String
    var imageIds: [String]?
    var stashScore: Int
}

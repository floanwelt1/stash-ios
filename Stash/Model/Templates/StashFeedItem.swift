//
//  StashFeedItem.swift
//  Stash
//
//  Created by Florian on 7/31/19.
//  Copyright © 2019 Florian Abel. All rights reserved.
//

import Foundation
import UIKit
import CoreLocation

class StashFeedItem {
    
    var id: Int
    var title: String
    var subtitle: String
    var category: String
    var location: CLLocation
    var distance: CLLocationDistance
    //var image: UIImage
    var starred: Bool
    
    init(id: Int, latitude: Double, longitude: Double, title: String) {
        self.id = id
        self.title = title
        self.subtitle = "subtitle"
        self.location = CLLocation(latitude: latitude, longitude: longitude)
        if let currentLocation = LocationProvider.shared.currentLocation {
            self.distance = self.location.distance(from: currentLocation)
        } else {
            self.distance = 0
        }
        self.category = "category"
        //self.image = image
        self.starred = false
    }
    
    func starItem() {
        if starred {
            starred = false
        } else if !starred {
            starred = true
        }
    }
    
}

//
//  LocationHelper.swift
//  Stash
//
//  Created by Florian on 7/17/19.
//  Copyright © 2019 Florian Abel. All rights reserved.
//

import Foundation
import CoreLocation


class LocationProvider: NSObject, CLLocationManagerDelegate {
    
    // ==========================================================================================
    // Variables and Constants
    // ==========================================================================================
    //

    static let shared = LocationProvider()
    
    private let locationManager = CLLocationManager()
    private weak var delegate: locationInteraction?
    private(set) var currentLocation: CLLocation?
    private var lastUpdatedLocation: CLLocation?
    private var currentCity: String?
    // private var regionRadius: CLLocationDistance = 5000
    
    
    // ==========================================================================================
    // Initialization
    // ==========================================================================================
    //

    private override init() {
        super.init()
        setupLocationManager()
    }
    
    // ==========================================================================================
    // Setup Entrance Points for Controller
    // ==========================================================================================
    //
    func setDelegate(_ delegate: locationInteraction) {
        self.delegate = delegate
    }
    
    // ==========================================================================================
    // Setup
    // ==========================================================================================
    //

    private func setupLocationManager() {
        switch CLLocationManager.authorizationStatus() {
        case .notDetermined:
            // Request when-in-use authorization initially
            // Ask for authorization from User for use in foreground
            self.locationManager.requestWhenInUseAuthorization()
            
            // Ask for Authorisation from User for use in background
            self.locationManager.requestAlwaysAuthorization()
            
            break
            
        case .restricted, .denied:
            // Disable location features
            //disableMyLocationBasedFeatures()
            break
            
        case .authorizedWhenInUse:
            // Enable basic location features
            //enableMyWhenInUseFeatures()
            break
            
        case .authorizedAlways:
            // Enable any of your app's location features
            //enableMyAlwaysFeatures()
            break
            
        default:
            break
        }
        
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.startUpdatingLocation()
            locationManager.requestLocation()
        }
    }
    
    
    // ==========================================================================================
    // Location Manager hooks
    // ==========================================================================================
    //
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let mostRecentLocation:CLLocation? = locations.last!
        updateLocation(location: mostRecentLocation)
    }
    
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        if let error = error as? CLError, error.code == .denied {
            // Location updates are not authorized.
            print("Location updates are not authorized.")
            manager.stopUpdatingLocation()
            return
        }
        // Notify user of errors
        print("An error occured in location Manager")
        print(error)
    }

    
    // ==========================================================================================
    // Logic
    // ==========================================================================================
    //
    
    private func updateLocation (location: CLLocation?) {
        guard let location = location else { return }
        
        if currentLocation == nil {
            DataSingleton.shared.updateData(location: location)
            lastUpdatedLocation = location
            print("Initial location successfully set")
        }
        
        if location != currentLocation {
            currentLocation = location
            if let lastUpdatedLocation = lastUpdatedLocation {
                let distanceFromLastUpdatedLocation = location.distance(from: lastUpdatedLocation)
                if distanceFromLastUpdatedLocation > 3 {
                    encodeLocation(location: location)
                    DataSingleton.shared.updateDistance(from: location)
                    self.lastUpdatedLocation = location
                    print("Successfully updated location")
                }
            }
        }
    }

    private func encodeLocation (location: CLLocation?) {
        CLGeocoder().reverseGeocodeLocation(location!) { (placemark, error) in
            if error == nil, let city = placemark?[0].locality {
                if self.currentCity != String(city) {
                    self.currentCity = city
                    self.delegate?.updateLocation(currentCity: city)
                }
            }
        }
    }
    
//    func setRadius(radiusInMetres: Int) {
//        regionRadius = CLLocationDistance(radiusInMetres)
//    }
    
}
